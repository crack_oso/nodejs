var express = require('express');
var app = express(); // import and run express as a function
var mysql = require('mysql');

var apiController = require('./controllers/apiController');
var htmlController = require('./controllers/htmlController');

var port = process.env.PORT || 3000; // will use env variable if it exists in a server, or just 3000 (locally)

app.use('/assets', express.static(__dirname + '/public'));

app.set('view engine', 'ejs');  // hooking up ejs templating engine, and the file extension

app.use('/', function(req, res, next) {
  console.log('Request URL:' + req.url);
  var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'node'
  });
  connection.query('SELECT * FROM people p INNER JOIN personAddresses a ON p.id = a.id', function(err, rows){
    if(err) throw err;
    console.log(rows[0].firstname);
    console.log(rows[0].lastname);
    console.log(rows[0].street);
    console.log(rows[0].city);
  });
  next();
});

apiController(app);
htmlController(app);

app.listen(port);  // running on port
