var bodyParser = require('body-parser');

module.exports = function(app){
  app.get('/api/person/:id', function(req, res) {
    res.json({firstname: 'Hibran', lastname: 'Martinez'});
  });

  app.post('/person', function(req, res) {
    //save to the database
  });

  app.delete('/person/:id', function(req, res) {
    //delete from the database
  });

}
