var bodyParser = require('body-parser');
var urlEncodedParser = bodyParser.urlencoded({
  extended: false
});

module.exports = function(app){
  //use setup, custom middleware (do stuff before the response):
  app.get('/', function (req, res, next) {
    res.render('person', { ID: req.params.id, Qstr: req.query.qstr });  // .query looks for value in request/URL
  });

  app.get('/person/:id', urlEncodedParser, function(req, res) {
    res.render('person', { ID: req.params.id, Qstr: req.query.qstr });
  });

  app.post('/person', urlEncodedParser, function(req, res) {
    res.send('gracias ');
    console.log(req.body.firstname);
    console.log(req.body.lastname);

  });
};
